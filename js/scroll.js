$(window).scroll(function () {
    if($(this).scrollTop() > 1){
        $('header').addClass('animation');
    } else {
        $('header').removeClass('animation');
    }

    if($(this).scrollTop() >= 0 && $(this).scrollTop() < 1377){
        $('#line').css({'margin-left': '265px'});
    }

    if($(this).scrollTop() > 1377 && $(this).scrollTop() < 3100){
        $('#line').css({'margin-left': '360px'});
    }

    if($(this).scrollTop() > 3100 && $(this).scrollTop() < 3700){
        $('#line').css({'margin-left': '440px'});
    }

    if($(this).scrollTop() > 3700 && $(this).scrollTop() < 5500){
        $('#line').css({'margin-left': '520px'});
    }

    if($(this).scrollTop() > 5500){
        $('#line').css({'margin-left': '609px'});
    }


});

$(document).ready(function () {
    $("#menu").on("click","a", function (event) {
        //отменяем стандартную обработку нажатия по ссылке
        event.preventDefault();
        //забираем идентификатор бока с атрибута href
        var id  = $(this).attr('href');
        //узнаем высоту от начала страницы до блока на который ссылается якорь
        var top = $(id).offset().top;
        //анимируем переход на расстояние - top за 1500 мс
        $('body,html').animate({scrollTop: top}, 1000);

    });

    $(".linkOnContact").click(function (event) {
        //отменяем стандартную обработку нажатия по ссылке
        event.preventDefault();
        //забираем идентификатор бока с атрибута href
        var id  = $(this).attr('href');
        //узнаем высоту от начала страницы до блока на который ссылается якорь
        var top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1000);

    });
});