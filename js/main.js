$(document).ready(function () {
    $("#nameAskForm").click(function () {
        $('#nameAskForm').addClass('opacityFormInput');
    }).blur(function () {
        $('#nameAskForm').removeClass('opacityFormInput');
    });
    
    $("#phoneAskForm").click(function () {
        $('#phoneAskForm').addClass('opacityFormInput');
    }).blur(function () {
        $('#phoneAskForm').removeClass('opacityFormInput');
    });

    $(".place").mouseenter(function () {

        if($(this.children).attr('class') !== 'animationImgPlaceRemove'){
             $(this.children).removeClass("animationImgPlaceRemove");
             $(this.children).addClass("animationImgPlace");
        } else {
             $(this.children).removeClass("animationImgPlaceRemove");
             $(this.children).addClass("animationImgPlace");
        }

        }).mouseleave(function () {
         if($(this.children).attr('class') === 'animationImgPlace'){
             $(this.children).removeClass("animationImgPlace");
             $(this.children).addClass("animationImgPlaceRemove");
         } else {
             $(this.children).removeClass("animationImgPlace");
             $(this.children).addClass("animationImgPlaceRemove");
         }
        });

    $("#formContact").submit(function (event) {

            var inputName = $("#nameContacts");
            var valueName = inputName.val();
            if(!/^[A-Za-zа-яА-ЯёЁ]+$/g.test(valueName)){
                event.preventDefault();
                inputName.val('');
                $("#spanUnderNameContacts").html("*Your name shouldn't have numbers and special symbols!");
            }
            if(valueName === ''){
                event.preventDefault();
                inputName.val('');
                $("#spanUnderNameContacts").html("*Please, input your name.");
            }
            if(/^[A-Za-zа-яА-ЯёЁ]+$/g.test(valueName)){
                $("#spanUnderNameContacts").html("");
            }

            var inputPhone = $("#phoneContacts");
            var valuePhone = inputPhone.val();
            if(!/^\+[0-9]{8,}$/g.test(valuePhone) && !/^[0-9]{8,}$/g.test(valuePhone)){
                event.preventDefault();
                inputPhone.val('');
                $("#spanUnderPhoneContacts").html("*Number should have only numbers and minimum length 8 symbols!");
            }
            if(valuePhone === ""){
                $("#spanUnderPhoneContacts").html("Please, input your phone.")
            }

            var inputEmail = $("#emailContacts");
            var valueEmail = inputEmail.val();
            if(!/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(valueEmail)){
                event.preventDefault();
                $("#spanUnderEmailContacts").html("*Please, input existing email.");
            }
            if(valueEmail === ""){
                event.preventDefault();
                $("#spanUnderEmailContacts").html("*Please, input email.");
            }

    });

    $("#AskForm").click(function(event){
        event.preventDefault();
        var nameAsk = $("#nameAskForm"), valName = nameAsk.val();
        var phoneAsk = $("#phoneAskForm"), valPhone = phoneAsk.val();

        $("#nameContacts").val(valName);
        $("#phoneContacts").val(valPhone);
    });


    $(".phoneContacts").keydown(function (event){

        if((event.keyCode <= 47 || event.keyCode >= 60 ) && event.keyCode !== 8){
            event.preventDefault();
        }

    });

    $(".nameContacts").keydown(function (event) {
        if(event.key >= 0 && event.key <= 9){
            event.preventDefault();
        }

    });


});


